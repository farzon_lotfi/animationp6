#ifndef APPS_SWING_IMAGE_PROCESSOR_H_
#define APPS_SWING_IMAGE_PROCESSOR_H_

#include <vector>
#include <opencv/cv.h>
#include <deque>

class ImageProcessor {
public:
    ImageProcessor();
    ~ImageProcessor() {}
    ImageProcessor(const ImageProcessor&);
    void setCurrentImage(std::vector<unsigned char>* image, int height, int width);
    void processImage();
    bool isPlatformIdeal(double floor, double ceil);
    bool isPlatformIdealBackLand();
    bool isPlatformIdealHeadLand();
    void diagnostics();
private:
    double computeAvgDistance();
    void addSpeedToRollingWindow(double avgDist);
    void findPlatformEdge(const cv::Mat &frame);
    //ImageProcessor(const ImageProcessor&) = delete;
    cv::Mat orangeFilter(const cv::Mat &frame);
    cv::Mat purpleFilter(const cv::Mat &frame);
    cv::Mat colorFilter(const cv::Mat &frame,cv::Scalar lowerBound,cv::Scalar upperBound);
    cv::Mat dilate_img(const cv::Mat &src,int dl_elem=2,int dl_size=5);
    std::vector<unsigned char>* currentImage = nullptr;
    int height=0, width=0, expectedSize=0;
    std::mutex mtx;
    int currSign;
    std::deque<int> pastRowPos;
    std::deque<double> pastSpeed;
    double platformCoverageInbounds;
    bool patformDirChange1, patformDirChange2;
};

#endif  // APPS_SWING_IMAGE_PROCESSOR_H_
