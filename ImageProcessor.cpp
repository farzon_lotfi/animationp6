#include "ImageProcessor.h"
#include <opencv2/opencv.hpp>

#define DEBUG1 0
#define DEBUG2 0
#define DEBUG3 0

#define ACCURATE 0

#define MAX_X 90
#define MAX_V 1000

ImageProcessor::ImageProcessor() {}
ImageProcessor::ImageProcessor(const ImageProcessor& imgP) {
    this->currentImage = imgP.currentImage;
    this->height = imgP.height;
    this->width = imgP.width;
    this->expectedSize = imgP.expectedSize;
    this->currSign = 0;
    patformDirChange1 = false;
    patformDirChange2 = false;
}

void ImageProcessor::setCurrentImage(std::vector<unsigned char>* image, int height, int width) {
    mtx.lock();
    this->currentImage = image;
    this->height = height;
    this->width = width;
    this->expectedSize = 4*height*width;
    //std::cout << "currentImage pixels: " << currentImage->size() << std::endl;
    mtx.unlock();
}
void ImageProcessor::processImage() {
    mtx.lock();
    if(this->currentImage == nullptr || currentImage->empty() ||
       currentImage->size() != expectedSize) {
        return;
    }
    cv::Mat src(width,height, CV_8UC4);
    memcpy(src.data, currentImage->data(), currentImage->size());
    cv::Mat flipped;               // dst must be a different Mat
    cv::flip(src, flipped, 0);
#if DEBUG1
    cv::Mat rgbaFrameDebug;
    cvtColor(flipped, rgbaFrameDebug, CV_BGRA2RGBA);
    //std::cout << "currentImage pixels: " << currentImage->size() << std::endl;
    imshow("sensor data", rgbaFrameDebug);
#endif

#if ACCURATE
    cv::Mat purplebgr = purpleFilter(flipped);
    cv::Mat rgbaFrame;
    cvtColor(purplebgr, rgbaFrame, CV_BGRA2RGBA);
    cv::Mat justTheFloor = purpleFilter(rgbaFrame);
#endif
    
#if DEBUG2
    imshow("floor filter",justTheFloor);
    //imshow("orange filter",orangeFilter(rgbaFrame));
    //cv::waitKey(0);
#endif

#if ACCURATE
    findPlatformEdge(justTheFloor);
#else
    findPlatformEdge(flipped);
#endif
    mtx.unlock();
}

int checkSign(int i)
{
    // returns -1 if it is negative
    // returns 0 if it is zero
    // returns 1 if it is positive
    return (i >> 31) - (-i >> 31);
}

double ImageProcessor::computeAvgDistance() {
    
    int totalDist = 0;
    int dSize = pastRowPos.size() - 1;
    for (int i = 0; i < dSize; i++) {
        totalDist += abs(pastRowPos[i+1] - pastRowPos[i]);
    }
    return totalDist /(double)(MAX_X-1);
}

void ImageProcessor::addSpeedToRollingWindow(double avgDist) {
    double speed = avgDist * pastSpeed.size();
    if (pastSpeed.size() == MAX_V) {
        pastSpeed.pop_front();
    }
    pastSpeed.push_back(speed);
}

void ImageProcessor::findPlatformEdge(const cv::Mat &frame) {
    static int lowerBound = 0;
    static int upperBound = this->height;
    int midCol = frame.cols/2;
    static int currRowIndex = 0;
    for (int r = frame.rows - 1; r >= 0; r--) {
        cv::Mat currentRow = frame.row(r);
        cv::Vec3b pixel = frame.at<cv::Vec3b>(r, midCol);
#if ACCURATE
        if(pixel.val[0] != 0 || pixel.val[1] != 0 || pixel.val[2] != 0) {
#else
        if (pixel.val[0] == 92 && pixel.val[1] == 82 && pixel.val[2] == 92) {
#endif
            currRowIndex = r;
            if (r > lowerBound)
                lowerBound = r;
                
            if (r < upperBound)
                upperBound = r;
            break;
        }
    }
    
    cv::Mat show = frame.clone();
    show.row(lowerBound) = cv::Scalar(0,255,0
#if !ACCURATE
                                      ,0
#endif
                                      );
    show.row(upperBound) = cv::Scalar(0,0,255
#if !ACCURATE
                                      ,0
#endif
                                      );
    imshow("show",show);
    
    if (pastRowPos.size() == MAX_X) {
        pastRowPos.pop_front();
    }
    if(pastRowPos.size() > 2) {
        int dx = pastRowPos.back() - pastRowPos.front();
        currSign = checkSign(dx);
    }
           
    if (pastSpeed.size() > 0) {
        int prevDx = pastSpeed.back();
        int checkSignChange = currSign*checkSign(prevDx);
        if(!patformDirChange1 && checkSignChange < 0){
            patformDirChange1 = true;
        }
         if(!patformDirChange2 && patformDirChange1 && checkSignChange < 0){
             patformDirChange2 = true;
         }
    }
    pastRowPos.push_back(currRowIndex);
    double avgDist  = computeAvgDistance();
    addSpeedToRollingWindow(avgDist);
    
    double deltaRow = (lowerBound - upperBound);
    if (deltaRow != 0) {
        platformCoverageInbounds = (currRowIndex - upperBound) / deltaRow;
    }
}

cv::Mat ImageProcessor::colorFilter(const cv::Mat &frame,cv::Scalar lowerBound,cv::Scalar upperBound) {
    cv::Mat hsv;
    cv::Mat mask;
    cv::Mat res;
    cv::Mat dilate;
    cv::cvtColor(frame, hsv, CV_BGR2HSV);
    cv::inRange(hsv, lowerBound, upperBound, mask);
    cv::bitwise_and(frame,frame,res,mask);
#if DEBUG3
    imshow("hsv", hsv);
    imshow("mask", mask);
    imshow("bitwise_and", res);
#endif
    return dilate_img(res);
}

cv::Mat ImageProcessor::orangeFilter(const cv::Mat &frame) {
    cv::Scalar lower_orange = cv::Scalar(5, 50, 50);
    cv::Scalar upper_orange = cv::Scalar(15, 255, 255);
    return colorFilter(frame, lower_orange, upper_orange);
}

cv::Mat ImageProcessor::purpleFilter(const cv::Mat &frame) {
    cv::Scalar lower_purple = cv::Scalar(60, 21, 87);
    cv::Scalar upper_purple = cv::Scalar(150, 255, 255);
    return colorFilter(frame, lower_purple, upper_purple);
}

cv::Mat ImageProcessor::dilate_img(const cv::Mat &src,int dl_elem,int dl_size) {
    int dilation_type = 0;
    cv::Mat dilation_dst;
    switch(dl_elem)
    {
        case 0:
            dilation_type = cv::MORPH_RECT;
            break;
        case 1:
            dilation_type = cv::MORPH_CROSS;
            break;
        case 2:
            dilation_type = cv::MORPH_ELLIPSE;
            break;
    }
    
    cv::Mat element = getStructuringElement( dilation_type,
                                        cv::Size( 2*dl_size + 1, 2*dl_size+1 ),
                                        cv::Point( dl_size, dl_size ) );
    /// Apply the dilation operation
    dilate( src, dilation_dst, element );
    return dilation_dst;
}

void ImageProcessor::diagnostics() {
    for (int i = 0; i < pastRowPos.size(); i++) {
        std::cout << "pastRowPos["<< i <<"] = " << pastRowPos[i] << std::endl;
    }
    for(int i = 0; i < pastSpeed.size(); i++) {
        std::cout << "pastSpeed["<< i <<"] = " << pastSpeed[i] << std::endl;
    }
}

bool ImageProcessor::isPlatformIdealHeadLand() {
    double floor = .52;
    double ceil = .60;
    return isPlatformIdeal(floor, ceil);
}

bool ImageProcessor::isPlatformIdealBackLand() {
    double floor = .70;
    double ceil = .80;
    return isPlatformIdeal(floor, ceil);
}

bool ImageProcessor::isPlatformIdeal(double floor, double ceil) {
    if(patformDirChange1 && patformDirChange2) {
        //std::cout << "platformCoverageInbounds: " << platformCoverageInbounds << std::endl;
        //std::cout << "currSign: " << currSign << std::endl;
        //std::cout << "pastSpeed.back(): " << pastSpeed.back() << std::endl;
        if(pastSpeed.back() > 0 && currSign == 1) {
            return platformCoverageInbounds > floor && platformCoverageInbounds < ceil;
        }
    }
    return false;
}
